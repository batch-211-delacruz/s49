// alert("We are going to simulate an interactive web page using DOM and fetching data from a server!");

/*
	fetch method
	Syntax:
	fetch('url',options)
	Accepts two arguments:
	url - this is the url which the request is to be made
	options parameter - used only when we need the details of the request from the user

	fetch() method in JavaScript is used to send requests in the server and load the received response in the webapage. The request and response is in JSON format
 

*/

//Get post data

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response)=>response.json())
.then((data)=>showPosts(data));
/*
	use fetch method to get the posts inside https://jsonplaceholder.typicode.com/posts
	make the response in json format (.then)
*/

//Add post data

document.querySelector('#form-add-post').addEventListener('submit',(e)=>{

	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts',{
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}),
		headers: {'Content-type':'application/json; charset=UTF-8'}
	})
	.then((response)=>response.json())//converts the response into JSON format
	.then((data)=>{
		console.log(data);
		alert('Successfully added.');
		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;
	})
})

/*Mini Activity
	Create a showPost function

*/
//Show Posts
const showPosts = (posts) =>{
	let postEntries = "";

	posts.forEach((post)=>{
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})
	console.log(postEntries)
	document.querySelector("#div-post-entries").innerHTML = postEntries
}

//Edit Post
const editPost = (id) =>{
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	//Pass the id, title, and body from the post to be updated in the edit post form
	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;

	//Add attribute
	document.querySelector('#btn-submit-update').removeAttribute('disabled');
}

//Update Post

document.querySelector('#form-edit-post').addEventListener('submit',(e)=>{

	e.preventDefault();
	fetch('https://jsonplaceholder.typicode.com/posts/1',{
		method: 'PUT',
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		}),
		headers: {'Content-type':'application/json; charset=UTF-8'}
	})
	.then((response)=>response.json())
	.then((data)=>{

		console.log(data);
		alert('Successfully updated.');

		//miniactivity:clear the input fields
		document.querySelector("#txt-edit-id").value = null;
		document.querySelector("#txt-edit-title").value = null;
		document.querySelector("#txt-edit-body").value = null;

		document.querySelector('#btn-submit-update').setAttribute('disabled',true);
	})
})

/*
	Activity:
	s49 Activity
		-Write the necessary code to delete a post
		-Make a function called deletePost.
		-Pass a parameter id or follow the typicode route for delete
		-Use fetch method and add the options
		-Send gif output in Batch Hangouts
		-Pass before 9:00PM
*/

let posts = []

const deletePost = (id) =>{
	posts = posts.filter((post)=>{
		if(post.id!=id){
			return post
		}
	})
	console.log(posts);
	document.querySelector(`#post-${id}`).remove();
};

//Solution 2
// const deletePost = (id)=>{
// 	for(let i=0;i<posts.length;i++){

// 		if(posts[i].id.toString()===id){
// 			posts.splice(i,1);
// 		}
// 	}
// 	showPosts(posts)
// 	alert('Post is deleted')
// }
